from collections.abc import AsyncIterator
from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import Header, HTTPException
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from db.models import Tenant
from db.multitenancy import tenant_context


@inject
async def get_tenant_context(
    session: Annotated[AsyncSession, Inject],
    tenant_name: Annotated[str, Header()],
) -> AsyncIterator[Tenant]:
    tenant = await session.scalar(select(Tenant).where(Tenant.name == tenant_name))
    if tenant is None:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)

    async with tenant_context(tenant, session):
        yield tenant
