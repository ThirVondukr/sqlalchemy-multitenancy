from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter, Body

from core.multitenancy.commands import TenantCreateCommand

router = APIRouter(
    prefix="/tenants",
    tags=["multitenancy"],
)


@router.post("")
@inject
async def tenants_create(
    name: Annotated[str, Body(embed=True)],
    command: Annotated[TenantCreateCommand, Inject],
) -> None:
    await command.execute(name=name)
