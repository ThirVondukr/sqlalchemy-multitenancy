import contextlib
import functools
from collections.abc import AsyncIterator, Callable
from contextvars import ContextVar
from typing import Protocol

from sqlalchemy import MetaData
from sqlalchemy.ext.asyncio import AsyncSession

from db._base import TENANT_SCHEMA
from db.models import Tenant

tenant_context_var = ContextVar[str]("tenant_schema")


class TenantFunction(Protocol):
    def __call__(self, schema: str) -> None:
        ...


def migration(func: Callable[[], None]) -> Callable[[], None]:
    @functools.wraps(func)
    def wrapper() -> None:
        try:
            tenant_context_var.get()
        except LookupError:
            return func()

    return wrapper


def tenant_migration(func: TenantFunction) -> Callable[[], None]:
    @functools.wraps(func)
    def wrapper() -> None:
        try:
            func(schema=tenant_context_var.get())
        except LookupError:
            return

    return wrapper


def get_tenant_meta(meta: MetaData, schema: str) -> MetaData:
    tenant_meta = MetaData()
    for table in meta.tables.values():
        if table.schema != TENANT_SCHEMA:
            continue
        table.to_metadata(tenant_meta, schema=schema)
    return tenant_meta


@contextlib.asynccontextmanager
async def tenant_context(
    tenant: Tenant | str,
    session: AsyncSession,
) -> AsyncIterator[None]:
    if isinstance(tenant, Tenant):
        tenant = tenant.schema

    conn = await session.connection()
    await conn.execution_options(schema_translate_map={TENANT_SCHEMA: tenant})
    yield
    await conn.execution_options(schema_translate_map={})
