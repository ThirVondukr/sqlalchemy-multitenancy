import aioinject

from core.multitenancy.commands import TenantCreateCommand
from core.multitenancy.services import MultiTenancyService
from core.types import Providers

providers: Providers = [
    aioinject.Callable(MultiTenancyService),
    aioinject.Callable(TenantCreateCommand),
]
