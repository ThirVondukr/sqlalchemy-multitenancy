"""
${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
from alembic import op
import sqlalchemy as sa
from db import multitenancy

${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


@multitenancy.migration
def upgrade() -> None:
    ${upgrades if upgrades else "pass"}


@multitenancy.migration
def downgrade() -> None:
    ${downgrades if downgrades else "pass"}
