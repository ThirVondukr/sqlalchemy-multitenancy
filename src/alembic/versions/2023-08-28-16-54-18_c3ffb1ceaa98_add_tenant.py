"""
Add tenant model

Revision ID: c3ffb1ceaa98
Revises: 4b281d647068
Create Date: 2023-08-28 16:54:18.070588

"""
import sqlalchemy as sa

from alembic import op
from db import multitenancy

# revision identifiers, used by Alembic.
revision = "c3ffb1ceaa98"
down_revision = "4b281d647068"
branch_labels = None
depends_on = None


@multitenancy.migration
def upgrade() -> None:
    op.create_table(
        "tenant",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.Column("schema", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_tenant")),
        sa.UniqueConstraint("schema"),
    )


@multitenancy.migration
def downgrade() -> None:
    op.drop_table("tenant")
